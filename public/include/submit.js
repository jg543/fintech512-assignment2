function doFunction(){
    bootbox.confirm({
        title: 'Submit Form?',
        message: 'Do you want to submit your form?',
        buttons: {
            cancel: {
                label: '<i class="fa fa-times"></i> Cancel'
            },
            confirm: {
                label: '<i class="fa fa-check"></i> Confirm'
            }
        },
        callback: function (result) {
            if (result == true) {
                bootbox.alert({
                    message: 'Form Submitted',
                    size: 'small'
                });
            }
            else{
                bootbox.alert({
                    message: 'Action Cancelled',
                    size: 'small'
                });
            }
        }
    });
}

